package ch.usi.dslab.bezerra.ridge.storage;

import java.util.Iterator;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public class FastArrayStorage implements Storage {

   public static class FastArrayBuffer<T> implements Iterable<T> {
      T[] buffer;
      private int nextPos;

      @SuppressWarnings("unchecked")
      public FastArrayBuffer(int capacity) {
         buffer = (T[]) new Object[capacity];
         nextPos = -1;
      }

      public void add(T item) {
         nextPos++;
         if (nextPos == buffer.length) nextPos = 0;
         buffer[nextPos] = item;
      }

      @Override
      public Iterator<T> iterator() {
         Iterator<T> it = new Iterator<T>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
               return currentIndex < buffer.length;
            }

            @Override
            public T next() {
               return buffer[currentIndex];
            }

            @Override
            public void remove() {
               throw new UnsupportedOperationException();
            }
         };
         return it;
      }

   }

   FastArrayBuffer<RidgeMessage> instances;

   public FastArrayStorage() {
      instances = new FastArrayBuffer<RidgeMessage>(Storage.CACHE_SIZE);
   }

   @Override
   public int putInstanceBatch(Long instanceId, RidgeMessage instanceBatch) {
      instances.add(instanceBatch);
      return 0;
   }

   @Override
   public RidgeMessage get(Long key) {
      for (RidgeMessage batch : instances)
         if (batch != null && batch.getInstanceId() == key)
            return batch;
      return null;
   }

}
