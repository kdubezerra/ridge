package ch.usi.dslab.bezerra.ridge.storage;

public class StorageBenchmark {
   
   public static void benchmark(Storage storage) {
      String storageName = storage.getClass().getName();
      long numWrites = Integer.MAX_VALUE;
      System.out.print(String.format("Benchmarking (%d writes) %s...", numWrites, storageName));
      long tStart = System.nanoTime();
      for (long i = 0 ; i < numWrites ; i++)
         storage.putInstanceBatch(i, null);
      long tEnd = System.nanoTime();
      System.out.println(String.format(" %f seconds", (tEnd - tStart)/1e9));
   }

   public static void main(String[] args) {
      String[] names = {"nostorage","fastarray","modarray","listcache","memcache"};
      for (String name : names) {
         Storage storage = StorageFactory.buildStorage(name, 0);
         benchmark(storage);
      }
   }

}
