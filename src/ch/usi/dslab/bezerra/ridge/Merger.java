/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.ridge.RidgeMessage.MessageIdentifier;
import ch.usi.dslab.bezerra.ridge.RidgeMessage.Timestamp;
import ch.usi.dslab.bezerra.ridge.optimistic.OptimisticMerger;
import ch.usi.dslab.bezerra.ridge.optimistic.OptimisticMergerCallback;
import ch.usi.dslab.bezerra.ridge.optimistic.ProcessLatencyEstimator;

public class Merger extends Thread implements OptimisticMergerCallback {
   
   static OptimisticMerger optMergerSDEBUG;
   
   public static double getInversionRate() {
      if (MulticastAgent.fastDelivery == true)
    	  return optMergerSDEBUG.getInversionRate();
      else
    	  return 0d;
   }
   
   Logger logger = LogManager.getLogger("Merger");
   
   Learner learner;
   
   Timestamp highestFastDeliveredTimestamp = Timestamp.ZERO;
   
   BlockingQueue<RidgeMessage> conservativePendingMessages;
   BlockingQueue<RidgeMessage> fastMessages;
   BlockingQueue<RidgeMessage> conservativeQueue;
   BlockingQueue<RidgeMessage> optimisticQueue;
   Map<MessageIdentifier, RidgeMessage> pendingMessagePayloads;
   OptimisticMerger optMerger;

   DeliverInterface deliverer;
   
   Semaphore mergeSignal;
   
   boolean running = true;
   
   Map<Integer, ProcessLatencyEstimator> processesLatencies = new ConcurrentHashMap<Integer, ProcessLatencyEstimator>();

   Merger(Learner learner) {
      super("Merger");
      
      this.learner = learner;
      
      conservativePendingMessages = new PriorityBlockingQueue<RidgeMessage>();
      conservativeQueue = new LinkedBlockingQueue<RidgeMessage>();
      optimisticQueue   = new LinkedBlockingQueue<RidgeMessage>();
      pendingMessagePayloads = new ConcurrentHashMap<MessageIdentifier, RidgeMessage>();
      if (MulticastAgent.fastDelivery == true)
    	  optMerger = new OptimisticMerger(this);
      
      mergeSignal = new Semaphore(0);
   }
   
   public void startRunning() {
      this.start();
   }
   
   public void setDeliverInterface(DeliverInterface deliverer) {
      this.deliverer = deliverer;
   }
   
   public void wakeUp() {
      mergeSignal.release();
   }
   
   private boolean waitForNewMessageSignal(long timeout_ms) {
      try {
         return mergeSignal.tryAcquire(timeout_ms, TimeUnit.MILLISECONDS);
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return false;
   }
   
   boolean assertMessagePayload(RidgeMessage m) {
      // if m was not sent fast, then its payload was forwarded by the Paxos acceptors
      if (MulticastAgent.fastDelivery == false)
         return true;
      
      RidgeMessage fastPayload = pendingMessagePayloads.remove(m.id);
      
      if (fastPayload == null) {
         return false;
      }
      
      m.setContents(fastPayload.getContents());
      return true;
   }

   private void mergeMessages() {
      logger.info("Running mergeMessages()");
      Timestamp maxDeliverableTimestamp = Timestamp.MAX_VALUE;
      List<RidgeMessage> partiallyMergedMessages = new ArrayList<RidgeMessage>();

      // this block merges all received messages from the different ensembles
      for (Ensemble e : learner.subscribedEnsembles) {
         Timestamp maxReadyEnsembleTimestamp = e.getLastCollectedTimestamp();
         logger.info("Ensemble {}'s max ready ts = {}", e.ensembleId, maxReadyEnsembleTimestamp);
         while (e.hasReadyMessage()) {
            
            RidgeMessage nextReadyMessageFromEnsemble = e.peekNextReadyMessage();
            
            if (nextReadyMessageFromEnsemble.isNull() || assertMessagePayload(nextReadyMessageFromEnsemble) == true) {
               logger.info("Successfully got a message for cons/opt delivery");
               
               // DEBUG
               nextReadyMessageFromEnsemble.t_learner_delivered = System.currentTimeMillis();
               // =====
               
               RidgeMessage confMessage = e.getNextReadyMessage();
               if (confMessage != nextReadyMessageFromEnsemble) {
                  System.out.println("concurrency error! peeked message != removed message!!!");
                  for (int iks = 0 ; iks < 40 ; iks++)
                     System.out.println("+-*-+-*-+-*-+-*-+-*-+");
                  System.exit(1);
               }
               maxReadyEnsembleTimestamp = nextReadyMessageFromEnsemble.timestamp;
               partiallyMergedMessages.add(nextReadyMessageFromEnsemble);
               

            }
            else {
               logger.info("Message has no payload ---");
               break;
            }
            
         }
         e.setLastCollectedTimestamp(maxReadyEnsembleTimestamp);
         maxDeliverableTimestamp = Timestamp.min(maxDeliverableTimestamp, maxReadyEnsembleTimestamp);
      }

      Collections.sort(partiallyMergedMessages);

      // this block duplicates all partially-merged messages,
      // for the optimistic delivery, and for the conservative one
      for (RidgeMessage nextMessage : partiallyMergedMessages) {
         if (nextMessage.isNull())
            continue;
         if (MulticastAgent.optimisticDelivery == true) {
            RidgeMessage optMessage = (RidgeMessage) nextMessage.deepDuplicate();
            optimisticQueue.add(optMessage);
         }
         if (MulticastAgent.conservativeDelivery == true) {
            RidgeMessage consMessage = nextMessage;
            conservativePendingMessages.add(consMessage);
         }
      }

      // this block checks which messages in the conservative-pending queue
      // are deliverable and put them in the conservative queue accordingly
      while (conservativePendingMessages.isEmpty() == false) {
         RidgeMessage nextMessage   = conservativePendingMessages.peek();
         Timestamp    nextTimestamp = nextMessage.timestamp;
         if (nextTimestamp.compareTo(maxDeliverableTimestamp) <= 0) {
            conservativePendingMessages.remove(nextMessage);
            synchronized (conservativeQueue) {
               conservativeQueue.add(nextMessage);
            }
         }
         else {
            break;
         }
      }
   }
   
   void addFastMessage(RidgeMessage message) {
      optMerger.addMessage((RidgeMessage) message.deepDuplicate());

      // if a message is sent opt/conservatively, the payload of the fast
      // delivery is *not* sent by the Paxos acceptors 
      if(MulticastAgent.conservativeDelivery || MulticastAgent.optimisticDelivery) {
         
         if (pendingMessagePayloads.containsKey(message.id)) {
            System.out.println("***************\n***************\n***************\nAdding id " +
                  message.id +
                  " to payload storage again!!!\n***************\n***************\n***************\n");
            System.exit(1);
         }
         
         pendingMessagePayloads.put(message.id, message);
         
      }
      
   }
   
   @Override
   public void uponOptimisticMerge(RidgeMessage message) {
      deliverer.deliverFast(message);
   }
   
   List<RidgeMessage> drainAllConservativelyDeliverableMessages() {
      synchronized (conservativeQueue) {
         List<RidgeMessage> allConsMessages = new ArrayList<RidgeMessage>(conservativeQueue.size());
         conservativeQueue.drainTo(allConsMessages);
         return allConsMessages;
      }
   }
   
   List<RidgeMessage> drainAllOptimisticallyDeliverableMessages() {
      synchronized (optimisticQueue) {
         List<RidgeMessage> allOptMessages = new ArrayList<RidgeMessage>(optimisticQueue.size());
         optimisticQueue.drainTo(allOptMessages);
         return allOptMessages;
      }
   }
   
   @Override
   public void run() {
      long waitTime = 1;
      while (running) {
         // wait for signal saying that there is a new message added
         // OR for a timeout
         boolean hasNewMessage = waitForNewMessageSignal(waitTime);
         
         if (hasNewMessage) {
            mergeMessages();

            List<RidgeMessage> optimisticMessages = drainAllOptimisticallyDeliverableMessages();
            for (RidgeMessage m : optimisticMessages) {
               m.rewind();
               logger.info("Delivering optimistically...");
               deliverer.deliverOptimistically(m);
            }

            List<RidgeMessage> conservativeMessages = drainAllConservativelyDeliverableMessages();
            for (RidgeMessage m : conservativeMessages) {
               m.rewind();
               logger.info("Delivering conservatively...");
               deliverer.deliverConservatively(m);
            }
         }
      }
   }
   
   public void stopRunning() {
      running = false;
   }
}
