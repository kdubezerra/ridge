/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.netwrapper.Message;
//import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;

public class Client extends Process {
   final int OUTSTANDING_PERMITS = 30;

   Logger logger = LogManager.getLogger("Client");
   
   MulticastAgent mcagent = null;
   
   int lastReqSeq = 0;   
   Map<Integer, Message> pendingRequests = new ConcurrentHashMap<Integer, Message>();
   List<Long> latencies         = new ArrayList<Long>();
   List<Long> latencyTimestamps = new ArrayList<Long>();
   long last_log_time = System.currentTimeMillis() + 5000l;
   long log_interval  = 100; // ms
   
   Semaphore outstandingPermits = new Semaphore(OUTSTANDING_PERMITS, true);
   
//   LatencyPassiveMonitor latencyMonitor;
   
   public Client(int id) {
      super (id, null, 0);
      mcagent = new MulticastAgent(this);
   }
   
   public MulticastAgent getMulticastAgent() {
      return mcagent;
   }
   
   @Override
   public void startRunning() {
//      latencyMonitor = new LatencyPassiveMonitor(this.pid, "client");
      super.startRunning();
   }
   
   public void connectToLearner(int learnerId) {
      Learner learner = (Learner) Process.getProcess(learnerId);
      connect(learner);
      sendCredentialsToLearner(learner);
   }
   
   void sendCredentialsToLearner(Learner l) {
      RidgeMessage credentials = new RidgeMessage();
      credentials.messageType = RidgeMessage.CLIENT_CREDENTIALS;
      credentials.addItems(this.pid);
      send(credentials, l);
   }
   
   @Override
   public void uponDelivery(Message m) {
      
      
      logger.info("Delivered something.");
      outstandingPermits.release();
      
      m.addItems("reply received by client");
      m.addItems(System.currentTimeMillis());
      
      long recvTimeNano = System.nanoTime();
      
      int reqSeq  = (Integer) m.getItem(0);
      Message req = pendingRequests.remove(reqSeq);
      
      
      
      if (req == null) {
         logger.fatal("Pending request " + reqSeq + " not found when processing its reply. Exiting...");
         System.exit(1);
      }
      
      long sendTimeNano = (Long) req.getItem(3);
      logger.info("Latency = {} ms", ((double) (recvTimeNano - sendTimeNano))/1e6);
//      latencyMonitor.logLatency(sendTimeNano, recvTimeNano);
      
      m.seek(1);
      while (m.hasNext())
         logger.info("{}:{}", m.getNext(), m.getNext());
   }
   
   void sendRequest(int ensembleId, byte[] rawMsg) {
      try {
         outstandingPermits.acquire();
         RidgeMessage msg = RidgeMessage.createClientRequestTSNow(pid, lastReqSeq, rawMsg, System.nanoTime(), "client send", System.currentTimeMillis());
         pendingRequests.put(lastReqSeq, msg);
         logger.debug("Sending request {}", lastReqSeq);
         lastReqSeq++;
         mcagent.multicast(msg, Ensemble.getEnsemble(ensembleId));
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      
   }
   
   void storeLatencies() {
      try {
         String log_dir = "/tmp/eduardo";
         new File(log_dir).mkdirs();
         Path latencyLogPath = Paths.get(log_dir, "latency_" + pid + ".log");
         BufferedWriter latWriter = Files.newBufferedWriter(latencyLogPath, StandardCharsets.UTF_8);
         latWriter.write("# ts latency");
         latWriter.newLine();
         for (int i = 0 ; i < latencies.size() ; i++) {
            latWriter.write(latencyTimestamps.get(i) + " " + latencies.get(i));
            latWriter.newLine();
         }
         latWriter.close();         
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   void runInteractive() {
      Scanner scan = new Scanner(System.in);
      String  input;      
      
      System.out.print("Input (end to exit): ");
      input = scan.nextLine();
      while (input.equalsIgnoreCase("end") == false) {
         int ensemble = Integer.parseInt(input.split(" ")[0]);
         String message = input.split(" ", 2)[1];
         System.out.println("message to send: " + message);
         this.sendRequest(ensemble, message.getBytes());
         System.out.print("Input (end to exit): ");
         input = scan.nextLine();
      }
      
      scan.close();
   }
   
   void runAutomatic(int duration) {
      final int  MSG_SIZE = 4096; // bytes
      byte[] request = new byte[MSG_SIZE];
      long start = System.currentTimeMillis();
      long end   = start + (duration * 1000l);
      
      System.out.println("Client " + this.pid + " sending requests...");
      
      while (System.currentTimeMillis() < end) {
         this.sendRequest(0, request);
      }
      try {
         this.outstandingPermits.acquire(this.OUTSTANDING_PERMITS);
      }
      catch (InterruptedException e) {
         e.printStackTrace(); System.exit(1);
      }
   }
   
   public static void main(String[] args) {

      // <command> id host port configFile duration
      // if configFile=="cmd"
      // <command> id host port   "cmd"    duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+

      int id = Integer.parseInt(args[0]);

/*
      String configFile = args[3];
      int duration = Integer.parseInt(args[4]);
      
      String gathererAddress = args[5];
      int    gathererPort    = Integer.parseInt(args[6]);

      if (configFile.equals("cmd"))
         Process.loadProcesses(Arrays.copyOfRange(args, 7, args.length));
      else
         Process.loadProcesses(configFile);


      DataGatherer.setDuration(duration);
//      DataGatherer.enableFileSave("/tmp/eduardo");
      DataGatherer.enableGathererSend(gathererAddress, gathererPort);
*/

      Process.loadProcesses();
      int duration = 0;
      Client cli = new Client(id);
      cli.startRunning();

//      final int  MSG_SIZE = 1000; // bytes
//      byte[] request = new byte[MSG_SIZE];
//      long start = System.currentTimeMillis();
//      long end   = start + (duration * 1000l);


      if (duration == 0) {
         cli.runInteractive();
      }
      else {
         cli.runAutomatic(duration);
      }
      
      cli.stop();
      System.out.println("Should have stopped the client...");

      System.out.println("Client " + cli.pid + " done. Storing latencies...");
      cli.storeLatencies();
      System.out.println("Client " + cli.pid + " exiting.");
            
   }

}
